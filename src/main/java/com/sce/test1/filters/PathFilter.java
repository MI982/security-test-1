package com.sce.test1.filters;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;
import org.springframework.web.servlet.NoHandlerFoundException;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerMapping;

@Component
public class PathFilter extends OncePerRequestFilter{
	
	private static final Logger log = LoggerFactory.getLogger(PathFilter.class);


	@Autowired
	private RequestMappingHandlerMapping mapper;
	
	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
			throws ServletException, IOException {
		
		String path = request.getRequestURI();
		log.info(path);
		
		try {
			if(mapper.getHandler(request)==null) {
				throw new NoHandlerFoundException(request.getMethod(), path, null);
			}
		} catch (Exception e) {
			log.info("Cought exception:"+e.getMessage());
			throw new ServletException(e);
		}
		
		filterChain.doFilter(request, response);
	}

}
