package com.sce.test1.security;

import javax.servlet.Filter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import com.sce.test1.filters.AuthenthicationFilter;
import com.sce.test1.filters.AuthorizationFilter;
import com.sce.test1.filters.MasterFilter;
import com.sce.test1.filters.PathFilter;

@EnableWebSecurity
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {
	
	@Autowired
	private PathFilter pathFilter;
	
	@Autowired
	private MasterFilter masterFilter;
	
	@Autowired
	private AuthorizationFilter authorizationFilter;
	
	@Autowired
	private AuthenthicationFilter authenticationFilter;

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		
		http.authorizeRequests()
			.antMatchers("/test/permitted", "/test/throwexception").permitAll()
			.antMatchers("/test/user").hasAnyAuthority("ROLE_USER", "ROLE_ADMIN")
			.antMatchers("/test/admin").hasAnyRole("ADMIN")
			.anyRequest().authenticated()
			.and()
			.addFilterAt(authenticationFilter, UsernamePasswordAuthenticationFilter.class)
			.addFilterBefore(authorizationFilter, authenticationFilter.getClass())
			.addFilterBefore(pathFilter, authorizationFilter.getClass())
			.addFilterBefore(masterFilter, pathFilter.getClass())
			.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
			;
	}
	
	

}
