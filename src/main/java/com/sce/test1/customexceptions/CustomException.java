package com.sce.test1.customexceptions;

import lombok.Getter;

@Getter
public class CustomException extends RuntimeException {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	private int status;
	private String message;
	
	public CustomException setStatus(int status) {
		this.status = status;
		return this;
	}
	
	public CustomException setMessage(String message) {
		this.message = message;
		return this;
	}

}
