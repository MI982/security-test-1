package com.sce.test1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ServletContainerExceptionTestApplication {

	public static void main(String[] args) {
		SpringApplication.run(ServletContainerExceptionTestApplication.class, args);
	}

}
