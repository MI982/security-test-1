package com.sce.test1.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.sce.test1.entities.User;

public interface UserRepository extends JpaRepository<User, Long>{

}
