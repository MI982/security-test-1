package com.sce.test1.controllers;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.NoHandlerFoundException;

import com.sce.test1.customexceptions.CustomException;
import com.sce.test1.dto.SimpleResponse;

@RestController
@RestControllerAdvice
public class GlobalExceptionHandler implements ErrorController{
	
	private static final Logger log = LoggerFactory.getLogger(GlobalExceptionHandler.class);
	
	
	private static final String ERROR_PATH = "/error";
	private int badRequest = HttpStatus.BAD_REQUEST.value();
	private int teapot = HttpStatus.I_AM_A_TEAPOT.value();
	private int forbidden = HttpStatus.FORBIDDEN.value();
	

	@ExceptionHandler(NoHandlerFoundException.class)
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	private SimpleResponse servletContainerExceptionHandler(NoHandlerFoundException exception, HttpServletResponse response) {
		log.info("It works!");
		return new SimpleResponse().setStatus(badRequest).setMessage(exception.getMessage());
		
	}
	
	@ExceptionHandler(CustomException.class)
	@ResponseStatus(HttpStatus.I_AM_A_TEAPOT)
	private SimpleResponse customExceptionHandler(CustomException exception, HttpServletResponse response) {
		return new SimpleResponse().setStatus(teapot).setMessage(exception.getMessage());
	}
	
	@ExceptionHandler(AccessDeniedException.class)
	@ResponseStatus(HttpStatus.FORBIDDEN)
	private SimpleResponse assessDeniedExceptionHandler(AccessDeniedException exception, HttpServletResponse response) {
		return new SimpleResponse().setStatus(forbidden).setMessage(exception.getMessage());
	}
	
	@ExceptionHandler(ServletException.class)
	private SimpleResponse servletExceptionHandler(ServletException exception, HttpServletResponse response) {
		log.info("ServletExceptionHandler resoved an exception!");
		SimpleResponse simpRes = new SimpleResponse();
		Exception ex = (Exception) exception.getCause();
		log.info("Servlet exception handler cought:"+exception.getMessage());
		
		if(ex instanceof NoHandlerFoundException) {
			simpRes.setStatus(badRequest).setMessage(ex.getMessage());
			response.setStatus(badRequest);
		}
		
		return simpRes;
	}
	
	@GetMapping("/error")
	public ResponseEntity<?> servletErrorHandler(Exception e, HttpServletRequest req, HttpServletResponse res) throws Exception {
		log.info("ServletErrorHandler resoved an error!");
		return ResponseEntity.status( (Integer) req.getAttribute(RequestDispatcher.ERROR_STATUS_CODE))
								.body(new SimpleResponse().setMessage((String)req.getAttribute(RequestDispatcher.ERROR_MESSAGE))
															.setStatus((Integer) req.getAttribute(RequestDispatcher.ERROR_STATUS_CODE)));
	}

	@Override
	public String getErrorPath() {
		return ERROR_PATH;
	}
}
