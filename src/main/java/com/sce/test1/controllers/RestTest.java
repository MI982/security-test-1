package com.sce.test1.controllers;

import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import com.sce.test1.customexceptions.CustomException;
import com.sce.test1.dto.RegisterUser;
import com.sce.test1.dto.SimpleResponse;

@RestController
public class RestTest {
	
	final static Logger log = LoggerFactory.getLogger(RestTest.class);
	
	@GetMapping("/test/permitted")
	public SimpleResponse allowedEndpoint(){
		return new SimpleResponse().setStatus(HttpServletResponse.SC_OK).setMessage("All's well.");
	}
	
	@GetMapping("/test/throwexception")
	public void throwException() {
		throw new CustomException().setMessage("Teapot!");
	}
	
	@GetMapping("/test/denied")
	public void denied() {
		
	}
	
	@GetMapping("/test/user")
	public SimpleResponse userAuthorized() {
		return new SimpleResponse().setStatus(HttpServletResponse.SC_OK).setMessage("User authorized!");
	}
	
	@GetMapping("/test/admin")
	public SimpleResponse adminAuthorized() {
		return new SimpleResponse().setStatus(HttpServletResponse.SC_OK).setMessage("Admin authorized!");
	}
	
	@PostMapping("/test/registeruser")
	public SimpleResponse registerUser(@RequestBody RegisterUser registerUser) {
		return null;
	}
	
	

}
