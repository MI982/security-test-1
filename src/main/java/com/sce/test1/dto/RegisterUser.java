package com.sce.test1.dto;

import org.springframework.stereotype.Component;

import lombok.Data;

@Component
@Data
public class RegisterUser {
	
	private String username, password;

}
