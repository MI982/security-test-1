package com.sce.test1.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class SimpleResponse {
	
	private int status;
	private String message;
	
	
	public SimpleResponse setStatus(int status) {
		this.status = status;
		return this;
	}
	
	public SimpleResponse setMessage(String message) {
		this.message = message;
		return this;
	}


}
